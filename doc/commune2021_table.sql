------------- SQLite3 Dump File -------------

-- ------------------------------------------
-- Dump of "commune2021"
-- ------------------------------------------

-- DROP TABLE IF EXISTS "commune2021";

CREATE TABLE IF NOT EXISTS "commune2021"(
	"id" Integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	"TYPECOM" Text NOT NULL,
	"COMM" Integer,
	"REG" Text,
	"DEP" Text,
	"CTCD" Text,
	"ARR" Text,
	"TNCC" Integer NOT NULL,
	"NCC" Text NOT NULL,
	"NCCENR" Text NOT NULL,
	"LIBELLE" Text NOT NULL,
	"CAN" Text,
	"CANPARENT" Text,
	"search" Text NOT NULL DEFAULT '' ,
	"cp" Text,
	"code_insee" Text,
	"superficie" real,
	"population" real,
	"altitude" real,
	"lat" real,
	"lng" real
);

CREATE INDEX "IDX_commune2021_REG" ON "commune2021"( "REG" );
CREATE INDEX "IDX_commune2021_NCCENR" ON "commune2021"( "NCCENR" );
CREATE INDEX "IDX_commune2021_NCC" ON "commune2021"( "NCC" );
CREATE INDEX "IDX_commune2021_LIBELLE" ON "commune2021"( "LIBELLE" );
CREATE INDEX "IDX_commune2021_DEP" ON "commune2021"( "DEP" );
CREATE INDEX "IDX_commune2021_COMM" ON "commune2021"( "COMM" );
CREATE INDEX "IDX_commune2021_search" ON "commune2021"( "search" );
CREATE INDEX "IDX_commune2021_cp" ON "commune2021"( "cp" );
CREATE INDEX "IDX_commune2021_code_insee" ON "commune2021"( "code_insee" );
CREATE INDEX "IDX_commune2021_lat" ON "commune2021"( "lat" );
CREATE INDEX "IDX_commune2021_lng" ON "commune2021"( "lng" );

