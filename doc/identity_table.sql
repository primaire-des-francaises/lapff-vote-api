BEGIN;

-- CREATE TABLE "identity" -------------------------------------
CREATE TABLE "identity"(
	"id" Text NOT NULL PRIMARY KEY,
	"name" Text NOT NULL,
	"firstNames" Text NOT NULL,
	"firstNameDisabled" Boolean NOT NULL,
	"gender" Text NOT NULL,
	"birthDate" Date NOT NULL,
	"birthDateIncomplete" Boolean NOT NULL,
	"nne" Text NOT NULL,
	"location" Text NOT NULL,
	"zip" Text NOT NULL,
	"createdAt" DateTime NOT NULL DEFAULT (datetime('now')),
	"updatedAt" DateTime NOT NULL DEFAULT (datetime('now')) )
WITHOUT ROWID;
-- -------------------------------------------------------------

-- CREATE INDEX "IDX_e8c0a5ce6d756c45a8b52beb88" ---------------
CREATE UNIQUE INDEX "IDX_e8c0a5ce6d756c45a8b52beb88" ON "identity"( "nne" );
-- -------------------------------------------------------------

COMMIT;