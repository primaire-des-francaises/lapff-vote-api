BEGIN;

-- CREATE TABLE "ballotBox" ------------------------------------
CREATE TABLE "ballotBox"(
	"id" Text NOT NULL PRIMARY KEY,
	"candidate" Text NOT NULL,
	"identityId" Text NOT NULL,
	"createdAt" DateTime NOT NULL DEFAULT (datetime('now')),
	"ip" Text,
	CONSTRAINT "FK_6d1a429e10c86347a5e14ad3651" FOREIGN KEY ( "identityId" ) REFERENCES "identity"( "id" )
		ON DELETE Cascade
		ON UPDATE Cascade
 );
-- -------------------------------------------------------------

-- CREATE INDEX "IDX_6d1a429e10c86347a5e14ad365" ---------------
CREATE UNIQUE INDEX "IDX_6d1a429e10c86347a5e14ad365" ON "ballotBox"( "identityId" );
-- -------------------------------------------------------------

PRAGMA foreign_keys=on;

COMMIT;