import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  ServiceUnavailableException,
} from '@nestjs/common';
import { VotesService } from './votes.service';
import { CreateVoteDto } from './dto/create-vote.dto';
import { UpdateVoteDto } from './dto/update-vote.dto';
import { RealIP } from 'nestjs-real-ip';

@Controller('votes')
export class VotesController {
  constructor(private readonly votesService: VotesService) {}

  isValidDate(d) {
    return d instanceof Date && !isNaN(d.getTime());
  }

  @Post('register')
  async voteRegister(@Body() vote: CreateVoteDto, @RealIP() ip: string) {
    const {
      gender,
      name,
      firstNameDisabled,
      firstNames,
      birthDateIncomplete,
      birthDate,
      location,
      zip,
      nne,
    } = vote.identity;

    const bDateYear = parseInt(birthDate.year);
    const bDateMonth = parseInt(birthDate.month);
    const bDateDay = birthDateIncomplete ? 1 : parseInt(birthDate.day);

    const bDate = new Date(bDateYear, bDateMonth, bDateDay);

    if (!this.isValidDate(bDate)) {
      throw new ServiceUnavailableException(
        `Error: birthDate() is not a valid date`,
        'ERROR_BIRTH_DATE_INVALID',
      );
    }

    let identity = {
      gender,
      name,
      firstNameDisabled,
      firstNames,
      birthDateIncomplete,
      birthDate: bDate,
      location,
      zip,
      nne,
    };

    return await this.votesService.voteRegister({
      identity,
      candidate: vote.candidate,
      ip: ip || '',
    });
  }

  // @Post()
  // create(@Body() createVoteDto: CreateVoteDto) {
  //   return this.votesService.create(createVoteDto);
  // }

  // @Get()
  // findAll() {
  //   return this.votesService.findAll();
  // }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.votesService.findOne(+id);
  // }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateVoteDto: UpdateVoteDto) {
  //   return this.votesService.update(+id, updateVoteDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.votesService.remove(+id);
  // }
}
