import { Identity } from 'src/identities/entities/identity.entity';

export class Vote {
  identity: Identity;

  candidate: string;
}
