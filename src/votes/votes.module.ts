import { Module } from '@nestjs/common';
import { VotesService } from './votes.service';
import { VotesController } from './votes.controller';
import { LocksModule } from 'src/locks/locks.module';
import { BallotBoxesModule } from 'src/ballot-boxes/ballot-boxes.module';
import { IdentitiesModule } from 'src/identities/identities.module';

@Module({
  controllers: [VotesController],
  providers: [VotesService],
  imports: [LocksModule],
})
export class VotesModule {}
