import { Injectable, ServiceUnavailableException } from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { BallotBox } from 'src/ballot-boxes/entities/ballot-box.entity';
import { LocksService } from 'src/locks/locks.service';
import { BaseEntity, Connection, Repository } from 'typeorm';
import { CreateVoteDto } from './dto/create-vote.dto';
import { UpdateVoteDto } from './dto/update-vote.dto';

import * as ormconfig from '../../ormconfig';
import { Identity } from 'src/identities/entities/identity.entity';
import { IdentitiesModule } from 'src/identities/identities.module';

export enum VoteStatus {
  success = 'success',
  failed = 'failed',
  failedHasVoted = 'failedHasVoted',
  failedDuplicateNne = 'failedDuplicateNne',
}

export class RegisterVoteResponse {
  status: VoteStatus;
}

@Injectable()
export class VotesService {
  constructor(
    @InjectConnection()
    private readonly connection: Connection,
    // @InjectRepository(BallotBox)
    // private readonly ballotBoxRepository: Repository<BallotBox>,
    // @InjectRepository(IdentitiesModule)
    // private readonly identityRepository: Repository<BallotBox>,
    private readonly locksService: LocksService,
  ) {}

  // voteStarts = new Date('2022-01-16T12:00:00+01:00');

  voteStarts = new Date('2022-01-16T00:00:00+01:00');
  voteEnds = new Date('2022-01-22T23:59:59+01:00');

  async identityMatch(id1: Identity, id2: Identity): Promise<Boolean> {
    return true;
  }

  async voteRegister({
    identity,
    candidate,
    ip = '',
  }: {
    identity: Identity;
    candidate: string;
    ip: string;
  }): Promise<RegisterVoteResponse> {
    let res: RegisterVoteResponse = { status: VoteStatus.failed };

    const findIdentity = await this.connection
      .getRepository(Identity)
      .createQueryBuilder('identity')
      .select()
      .leftJoinAndSelect('identity.ballotBoxes', 'ballotBoxes')
      .where({ nne: identity.nne })
      .getOne();

    // console.log(findIdentity);

    if (findIdentity) {
      if (this.identityMatch(findIdentity, identity)) {
        const hasVoted =
          findIdentity.ballotBoxes && findIdentity.ballotBoxes.length;

        if (hasVoted) {
          res.status = VoteStatus.failedHasVoted;
          return res;
        }
      } else {
        res.status = VoteStatus.failedDuplicateNne;
        return res;
      }

      // We have a matching Identity and no vote registered
      identity = findIdentity;
    }

    const voteTransaction = await this.voteRegisterTransaction({
      identity,
      candidate,
      ip,
    });

    res.status = VoteStatus.success;

    // .catch((error) => {
    //   const err = 'ERROR_FAILED_REGISTER_VOTE';

    //   console.error(error);

    //   // throw new ServiceUnavailableException(
    //   //   'Failed to register the Vote. Please try again later.',
    //   //   err,
    //   // );

    //   throw new ServiceUnavailableException(error);
    // });

    return res;
  }

  private async voteRegisterTransaction({
    identity,
    candidate,
    ip = '',
  }: {
    identity: Identity;
    candidate: string;
    ip: string;
  }) {
    await this.locksService.lock.acquire(
      'connection',
      async () => {
        // await this.doVoteTransaction({ identity, candidate });
        await this.connection.transaction(async (voteTransaction) => {
          let insertIdentity;
          let insertBallotBox;
          let ballotBox: BallotBox;

          if (!identity.id) {
            // we need to register a Identity
            try {
              insertIdentity = await voteTransaction
                .createQueryBuilder()
                .insert()
                .into(Identity)

                .values(identity)

                // .returning('*')
                .execute();

              // identity.id = insertIdentity.identifiers[0].id;

              // console.log(
              //   'insertIdentity.generatedMaps[0]',
              //   insertIdentity.generatedMaps[0],
              // );
              // console.log(
              //   'insertIdentity.identifiers',
              //   insertIdentity.identifiers,
              // );

              identity.id = (<Identity>insertIdentity.generatedMaps[0]).id;
            } catch (error) {
              const err = 'ERROR_FAILED_REGISTER_IDENTITY';

              throw new ServiceUnavailableException(
                'Failed to register the Identity. Please try again later.',
                err,
              );
            }
          }

          try {
            insertBallotBox = await voteTransaction
              .createQueryBuilder()
              .insert()
              .into(BallotBox)

              .values({
                identityId: identity.id,
                candidate,
                ip: ip || '',
              })

              // .returning('*')
              .execute();

            // console.log(
            //   'insertBallotBox.generatedMaps',
            //   insertBallotBox.generatedMaps,
            // );

            ballotBox = insertBallotBox.generatedMaps[0];
          } catch (error) {
            const err = 'ERROR_FAILED_REGISTER_BALLOT_BOX';

            throw new ServiceUnavailableException(
              'Failed to register the vote in BallotBox. Please try again later.',
              err,
            );
          }

          return { identity, ballotBox };
        });
      },
      {
        timeout: 30 * 1000,
      },
    );
  }

  create(createVoteDto: CreateVoteDto) {
    return 'This action adds a new vote';
  }

  findAll() {
    return `This action returns all votes`;
  }

  findOne(id: number) {
    return `This action returns a #${id} vote`;
  }

  update(id: number, updateVoteDto: UpdateVoteDto) {
    return `This action updates a #${id} vote`;
  }

  remove(id: number) {
    return `This action removes a #${id} vote`;
  }
}
