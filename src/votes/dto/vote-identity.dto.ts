export class VoteIdentity {
  name: string;

  firstNames: string[];

  firstNameDisabled: boolean;

  gender: string;

  birthDate: {
    day: string;
    month: string;
    year: string;
  };

  birthDateIncomplete: boolean;

  nne: string;

  location: string;

  zip: string;
}
