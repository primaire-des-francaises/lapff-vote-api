import { VoteIdentity } from './vote-identity.dto';

export class CreateVoteDto {
  identity: VoteIdentity;

  candidate: string;
}
