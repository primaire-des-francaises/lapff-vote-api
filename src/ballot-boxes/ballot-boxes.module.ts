import { Module } from '@nestjs/common';
import { BallotBoxesService } from './ballot-boxes.service';
import { BallotBoxesController } from './ballot-boxes.controller';

@Module({
  controllers: [BallotBoxesController],
  providers: [BallotBoxesService]
})
export class BallotBoxesModule {}
