import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Type } from 'class-transformer';
import { Identity } from 'src/identities/entities/identity.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  Generated,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('ballotBox', { withoutRowid: true })
export class BallotBox {
  @Column({
    type: 'uuid',
    primary: true,
  })
  @Generated('uuid')
  id: string;

  @Column({ type: 'text' })
  candidate: string;

  @Column({
    type: 'text',
    nullable: true,
  })
  ip: string;

  @Index({ unique: true })
  @Column({
    type: 'uuid',
    select: false,
  })
  @Exclude()
  identityId: string;

  @ManyToOne(() => Identity, (identity) => identity.ballotBoxes, {
    cascade: false,
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'identityId' })
  identity: Identity;

  @ApiProperty({
    default: 'CURRENT_TIMESTAMP',
    readOnly: true,
    required: false,
  })
  @CreateDateColumn({ type: 'datetime' })
  @Type(() => Date)
  createdAt?: Date;
}
