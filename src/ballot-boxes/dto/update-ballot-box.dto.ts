import { PartialType } from '@nestjs/swagger';
import { CreateBallotBoxDto } from './create-ballot-box.dto';

export class UpdateBallotBoxDto extends PartialType(CreateBallotBoxDto) {}
