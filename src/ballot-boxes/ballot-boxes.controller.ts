import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { BallotBoxesService } from './ballot-boxes.service';
import { CreateBallotBoxDto } from './dto/create-ballot-box.dto';
import { UpdateBallotBoxDto } from './dto/update-ballot-box.dto';

@Controller('ballot-boxes')
export class BallotBoxesController {
  constructor(private readonly ballotBoxesService: BallotBoxesService) {}

  @Post()
  create(@Body() createBallotBoxDto: CreateBallotBoxDto) {
    return this.ballotBoxesService.create(createBallotBoxDto);
  }

  @Get()
  findAll() {
    return this.ballotBoxesService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.ballotBoxesService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateBallotBoxDto: UpdateBallotBoxDto,
  ) {
    return this.ballotBoxesService.update(+id, updateBallotBoxDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.ballotBoxesService.remove(+id);
  }
}
