import { Injectable } from '@nestjs/common';
import { CreateBallotBoxDto } from './dto/create-ballot-box.dto';
import { UpdateBallotBoxDto } from './dto/update-ballot-box.dto';

@Injectable()
export class BallotBoxesService {
  create(createBallotBoxDto: CreateBallotBoxDto) {
    return 'This action adds a new ballotBox';
  }

  findAll() {
    return `This action returns all ballotBoxes`;
  }

  findOne(id: number) {
    return `This action returns a #${id} ballotBox`;
  }

  update(id: number, updateBallotBoxDto: UpdateBallotBoxDto) {
    return `This action updates a #${id} ballotBox`;
  }

  remove(id: number) {
    return `This action removes a #${id} ballotBox`;
  }
}
