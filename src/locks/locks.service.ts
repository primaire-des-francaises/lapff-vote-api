import { Injectable } from '@nestjs/common';
import * as AsyncLock from 'async-lock';

type LockKey = 'connection';

@Injectable()
export class LocksService {
  constructor() {
    this.lock = new AsyncLock();
  }
  public readonly lock: AsyncLock;

  // public acquire;
  // public isBusy;
}
