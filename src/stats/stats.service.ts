import { Injectable } from '@nestjs/common';
import { InjectConnection } from '@nestjs/typeorm';
import { BallotBox } from 'src/ballot-boxes/entities/ballot-box.entity';
import { Identity } from 'src/identities/entities/identity.entity';
import { Connection } from 'typeorm';
import { CreateStatDto } from './dto/create-stat.dto';
import { UpdateStatDto } from './dto/update-stat.dto';

@Injectable()
export class StatsService {
  constructor(
    @InjectConnection()
    private readonly connection: Connection,
  ) {}

  async counter(votesCount: boolean = false) {
    const date = `
    DATE(
      SUBSTR( ballotBox.createdAt, 1, 4 )
      || '-' || SUBSTR( ballotBox.createdAt, 6, 2 ) 
      || '-' || SUBSTR( ballotBox.createdAt, 9, 2 )
    )
    `;

    let result: any = {
      total: await this.connection
        .getRepository(Identity)
        .createQueryBuilder('identity')
        .leftJoin('identity.ballotBoxes', 'ballotBox')

        .select('COUNT(*)', 'identities')
        .addSelect('COUNT(ballotBox.id)', 'votes')

        // .addSelect(
        //   'COUNT(NULLIF(ballotBox.candidate = :blank, FALSE))',
        //   'blank',
        // )
        // .setParameter('blank', 'Vote Blanc')

        .getRawOne(),

      daily: await this.connection
        .getRepository(BallotBox)
        .createQueryBuilder('ballotBox')

        .select('COUNT(ballotBox.id)', 'votes')
        .addSelect(date, 'date')
        .addOrderBy(date, 'DESC')

        // .addSelect(
        //   'COUNT(NULLIF(ballotBox.candidate = :blank, FALSE))',
        //   'blank',
        // )
        // .setParameter('blank', 'Vote Blanc')

        .groupBy(date)

        .getRawMany(),
    };

    if (votesCount) {
      result.votes = await this.connection
        .getRepository(BallotBox)
        .createQueryBuilder('ballotBox')

        .select('ballotBox.candidate', 'candidate')
        .addSelect('COUNT(ballotBox.candidate)', 'count')

        .innerJoin('ballotBox.identity', 'identity')

        .addOrderBy('COUNT(ballotBox.candidate)', 'DESC')
        .addOrderBy('ballotBox.candidate', 'ASC')

        .groupBy('ballotBox.candidate')
        .getRawMany();
    }

    return result;
  }

  // create(createStatDto: CreateStatDto) {
  //   return 'This action adds a new stat';
  // }

  // findAll() {
  //   return `This action returns all stats`;
  // }

  // findOne(id: number) {
  //   return `This action returns a #${id} stat`;
  // }

  // update(id: number, updateStatDto: UpdateStatDto) {
  //   return `This action updates a #${id} stat`;
  // }

  // remove(id: number) {
  //   return `This action removes a #${id} stat`;
  // }
}
