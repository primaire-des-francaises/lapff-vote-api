import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Connection } from 'typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CommunesModule } from './communes/communes.module';
import { IdentitiesModule } from './identities/identities.module';
import { VotesModule } from './votes/votes.module';
import { BallotBoxesModule } from './ballot-boxes/ballot-boxes.module';
import { StatsModule } from './stats/stats.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRoot(),
    CommunesModule,
    IdentitiesModule,
    VotesModule,
    BallotBoxesModule,
    StatsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private connection: Connection) {}
}
