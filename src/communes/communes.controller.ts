import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Req,
} from '@nestjs/common';
import { Request } from 'express';
import { CommunesService } from './communes.service';
import { CreateCommuneDto } from './dto/create-commune.dto';
import { UpdateCommuneDto } from './dto/update-commune.dto';

@Controller('communes')
export class CommunesController {
  constructor(private readonly communesService: CommunesService) {}

  @Post()
  create(@Body() createCommuneDto: CreateCommuneDto) {
    return this.communesService.create(createCommuneDto);
  }

  @Get()
  search(@Req() req: Request) {
    let q: string = req.query.q as string;
    const limit: string = req.query.limit as string;

    let params: any = {};

    if (req.query.q) params.q = q;
    if (req.query.limit) params.limit = limit;

    return this.communesService.search(params);
  }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.communesService.findOne(+id);
  // }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateCommuneDto: UpdateCommuneDto) {
  //   return this.communesService.update(+id, updateCommuneDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.communesService.remove(+id);
  // }

  // @Get(':id')
  // updateSearch() {
  //   return this.communesService.updateSearchField();
  // }
}
