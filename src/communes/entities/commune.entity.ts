import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('commune2021')
export class Commune {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({
    type: 'text',
  })
  TYPECOM: string;

  @Index({ unique: false })
  @Column({
    type: 'integer',
    nullable: true,
  })
  COMM: number;

  @Index({ unique: false })
  @Column({
    type: 'text',
    nullable: true,
  })
  REG: string;

  @Index({ unique: false })
  @Column({
    type: 'text',
    nullable: true,
  })
  DEP: string;

  @Column({
    type: 'text',
    nullable: true,
  })
  CTCD: string;

  @Column({
    type: 'text',
    nullable: true,
  })
  ARR: string;

  @Column({
    type: 'integer',
  })
  TNCC: number;

  @Index({ unique: false })
  @Column({
    type: 'text',
  })
  NCC: string;

  @Index({ unique: false })
  @Column({
    type: 'text',
  })
  NCCENR: string;

  @Index({ unique: false })
  @Column({
    type: 'text',
  })
  LIBELLE: string;

  @Column({
    type: 'text',
    nullable: true,
  })
  CAN: string;

  @Column({
    type: 'text',
    nullable: true,
  })
  CANPARENT: string;

  @Index({ unique: false })
  @Column({
    type: 'text',
    nullable: false,
  })
  search: string;

  @Index({ unique: false })
  @Column({
    type: 'text',
    nullable: true,
  })
  cp: string;

  @Index({ unique: false })
  @Column({
    type: 'text',
    nullable: true,
  })
  code_insee: string;

  @Column({
    type: 'real',
    nullable: true,
  })
  superficie: number;

  @Column({
    type: 'real',
    nullable: true,
  })
  population: number;

  @Column({
    type: 'real',
    nullable: true,
  })
  altitude: number;

  @Index({ unique: false })
  @Column({
    type: 'real',
    nullable: true,
  })
  lat: number;

  @Index({ unique: false })
  @Column({
    type: 'real',
    nullable: true,
  })
  lng: number;

  @OneToMany(() => Commune, (child) => child.parent)
  // @JoinColumn({ name: 'CAN', referencedColumnName: 'CANPARENT' })
  children: Commune[];

  @ManyToOne(() => Commune, (parent) => parent.children, {
    cascade: false,
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION',
    primary: false,
    createForeignKeyConstraints: false,
  })
  @JoinColumn({ referencedColumnName: 'CAN', name: 'CANPARENT' })
  parent: Commune;
}
