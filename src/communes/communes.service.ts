import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { query } from 'express';
import {
  Brackets,
  Connection,
  EntityManager,
  getConnection,
  Like,
  Not,
  Repository,
} from 'typeorm';
import { CreateCommuneDto } from './dto/create-commune.dto';
import { UpdateCommuneDto } from './dto/update-commune.dto';
import { Commune } from './entities/commune.entity';

const stripAccents = (str: string): string => {
  return str.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
};

export const rawQuery = (
  query: string,
  parameters: object = {},
  manager?: EntityManager,
): [string, any[]] => {
  const conn = manager ? manager.connection : getConnection();
  const [escapedQuery, escapedParams] = conn.driver.escapeQueryWithParameters(
    query,
    parameters,
    {},
  );
  // return conn.query(escapedQuery, escapedParams);
  return [escapedQuery, escapedParams];
};

@Injectable()
export class CommunesService {
  constructor(
    @InjectRepository(Commune)
    readonly repo: Repository<Commune>,
    private connection: Connection,
  ) {}

  async search({
    q,
    limit,
  }: {
    q?: string;
    limit?: number;
  }): Promise<Commune[]> {
    const req = this.repo.createQueryBuilder().select();

    req.where('TRUE');
    req.andWhere('NCC NOT IN (:...excludeTowns)', {
      excludeTowns: ['PARIS', 'LYON', 'MARSEILLE'],
    });

    req.andWhere('TYPECOM != :COMD', { COMD: 'COMD' });

    q = q ? String(q).trim() : undefined;

    const qClean = q
      ? stripAccents(q.toUpperCase())
          .replace(/[^A-Z0-9 ]+/g, ' ')
          .replace(/\s+/g, ' ')
          .trim()
      : '';

    if (q) {
      // const splitRegex = /[\s'"\-_%]+/g;
      const qa = qClean.split(/\s+/);

      if (qa.length) {
        const w = new Brackets((qb) => {
          qb.where('TRUE');

          qa.forEach((str, index) => {
            // str = stripAccents(str.toUpperCase()).replace(
            //   /[^A-Z0-9' \-]+/g,
            //   ' ',
            // );

            const qWord = `qWord_${index}`;

            // req.setParameter(qWord, str);

            if (parseInt(str) || /0/.test(str)) {
              const w2 = new Brackets((qb2) => {
                qb2.where({ DEP: Like(`%${str}%`) });
                // qb2.where(`DEP LIKE ('%' || :${qWord} || '%')`);
                qb2.orWhere({ search: Like(`%${str}%`) });
                // qb2.orWhere(`search LIKE ('%' || :${qWord} || '%')`);
              });

              qb.andWhere(w2);
            } else {
              qb.andWhere({ search: Like(`%${str}%`) });
              // qb.andWhere(`search LIKE ('%' || :${qWord} || '%')`);
            }
            qb;
          });
        });

        req.andWhere(w);
      }

      if (qClean) {
        req
          .addOrderBy(`search LIKE :q`, 'DESC')
          .addOrderBy(`NCC LIKE :q`, 'DESC')
          .addOrderBy(`search LIKE (:q || '%')`, 'DESC')
          .addOrderBy(`NCC LIKE (:q || '%')`, 'DESC')
          .addOrderBy('population > 2', 'DESC')
          .addOrderBy(`search LIKE ('%' || :qWildCard || '%')`, 'DESC')

          .setParameter('q', qClean)
          .setParameter('qWildCard', qClean.split(/\s/).join('%'));
      }

      // const sqlParam = await this.connection.driver.escapeQueryWithParameters(
      //   'NCC LIKE :string',
      //   { string: q },
      //   {},
      // );
    }

    if (limit) req.limit(Math.max(limit, 0));

    // req.addOrderBy(`NCC`, 'ASC');
    req.addOrderBy(`id`, 'ASC');

    return await req.getMany();
  }

  // async updateSearchField(): Promise<number> {
  //   const communes = await this.repo
  //     .createQueryBuilder()
  //     .select()
  //     .where({ search: '' })
  //     // .limit(10)
  //     .getMany();

  //   for (let commune of communes) {
  //     commune.search = stripAccents(
  //       commune.LIBELLE.replace(/['\-]+/g, ' ').toUpperCase(),
  //     );
  //     await this.repo.save(commune);
  //   }

  //   return communes.length;
  // }

  create(createCommuneDto: CreateCommuneDto) {
    return 'This action adds a new commune';
  }

  async findAll(): Promise<Commune[]> {
    const req = this.repo.createQueryBuilder().select();

    return await req.execute();
  }

  findOne(id: number) {
    return `This action returns a #${id} commune`;
  }

  update(id: number, updateCommuneDto: UpdateCommuneDto) {
    return `This action updates a #${id} commune`;
  }

  remove(id: number) {
    return `This action removes a #${id} commune`;
  }
}
