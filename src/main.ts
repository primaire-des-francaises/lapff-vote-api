import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  // const configService = app.get(ConfigService);

  app.enableCors({
    origin: '*',
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
    credentials: true,
  });

  // app.enableCors({
  //   origin: true,
  //   methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
  //   credentials: true,
  // });

  const swaggerOptions = new DocumentBuilder()
    .setTitle('La PFF Vote API')
    .setContact(
      'La PFF Vote API',
      'https://gitlab.com/primaire-des-francaises/lapff-vote-api',
      'admin@article3.net',
    )
    .setDescription('API for a voting platform')
    .setVersion(process.env.npm_package_version)
    .addTag('La PFF Vote')
    .addBearerAuth(
      { type: 'http', scheme: 'bearer', bearerFormat: 'JWT' },
      'access-token',
    )
    .build();

  const swaggerDocument = SwaggerModule.createDocument(app, swaggerOptions);
  SwaggerModule.setup('swagger', app, swaggerDocument);

  const PORT = Number(process.env.PORT) || 3000;
  await app.listen(PORT).then((server) => {
    console.log(`Server listening on http://localhost:${PORT}/`);
  });
}
bootstrap();
