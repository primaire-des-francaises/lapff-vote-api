import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { BallotBox } from 'src/ballot-boxes/entities/ballot-box.entity';
import { Vote } from 'src/votes/entities/vote.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  Generated,
  Index,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('identity', { withoutRowid: true })
export class Identity {
  @Column({
    type: 'uuid',
    primary: true,
  })
  @Generated('uuid')
  id?: string;

  @Column({
    type: 'text',
  })
  name: string;

  @Column({
    type: 'simple-array',
  })
  firstNames: string[];

  @Column({ type: 'boolean' })
  firstNameDisabled: boolean;

  @Column()
  gender: string;

  @Column({ type: 'date' })
  birthDate: Date;

  @Column({ type: 'boolean' })
  birthDateIncomplete: boolean;

  @Index({ unique: true })
  @Column({
    type: 'text',
  })
  nne: string;

  @Column({
    type: 'text',
  })
  location: string;

  @Column({
    type: 'text',
  })
  zip: string;

  @OneToMany(() => BallotBox, (ballotBox) => ballotBox.identity)
  ballotBoxes?: BallotBox[];

  @ApiProperty({
    default: 'CURRENT_TIMESTAMP',
    readOnly: true,
    required: false,
  })
  @CreateDateColumn({ type: 'datetime' })
  @Type(() => Date)
  createdAt?: Date;

  /**
   * Auto-updated at insert & update
   *
   * @type {Date}
   * @memberof ControlPlace
   */

  @ApiProperty({
    default: 'CURRENT_TIMESTAMP',
    readOnly: true,
    required: false,
  })
  @UpdateDateColumn({ type: 'datetime' })
  @Type(() => Date)
  updatedAt?: Date;
}
