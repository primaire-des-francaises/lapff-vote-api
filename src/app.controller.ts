import { Controller, Get } from '@nestjs/common';

import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  // @Get()
  // getHello(): string {
  //   return this.appService.getHello();
  // }

  @Get('ping')
  ping() {
    const now = new Date();
    return {
      utc: now.toISOString().slice(0, -5) + 'Z',
      paris: now.toLocaleString('fr-fr', {
        timeZone: 'Europe/Paris',
      }),
    };
  }
}
